import { createApp } from "vue";
import App from "./App.vue";
import ElementPlus from "element-plus";
import VueAxios from "vue-axios";
import axios from "axios";
import "element-plus/dist/index.css"

createApp(App)
    .use(VueAxios, axios)
    .use(ElementPlus)
    .mount("#app");
