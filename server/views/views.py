from fastapi import APIRouter
from typing import List, Optional
from models.models import (
    Todo,
    TodoIn_pydantic,
    Todo_pydantic
)
from pydantic import BaseModel
from tortoise.contrib.fastapi import  HTTPNotFoundError
from fastapi.exceptions import HTTPException



class Status(BaseModel):
    message: str
    

app_router = APIRouter()

@app_router.get('/')
async def main_root():
    return {
        "test": 1
    }


@app_router.get('/todos', response_model=List[Todo_pydantic])
async def get_todo():
    return await Todo_pydantic.from_queryset(Todo.all().order_by('id'))


@app_router.get('/todos/{todo_id}', responses={404: {"model": HTTPNotFoundError}})
async def get_single_todo(todo_id: int):
    return await Todo_pydantic.from_queryset_single(Todo.get(id=todo_id))


@app_router.post('/todos', response_model=Todo_pydantic)
async def create_todo(todo: TodoIn_pydantic):
    todo_obj = await Todo.create(**todo.dict(exclude_unset=True))
    return await Todo_pydantic.from_tortoise_orm(todo_obj)


@app_router.put('/todos/{todo_id}', response_model=Todo_pydantic, responses={404: {"model": HTTPNotFoundError}})
async def update_todo(todo_id: int, todo: TodoIn_pydantic):
    await Todo.filter(id=todo_id).update(**todo.dict(exclude={"id"}, exclude_unset=True))
    return await Todo_pydantic.from_queryset_single(Todo.get(id=todo_id))


@app_router.delete('/todos/{todo_id}', response_model=Status, responses={404: {"model": HTTPNotFoundError}})
async def delete_todo(todo_id: int):
    delete_count = await Todo.filter(id=todo_id).delete()
    if not delete_count:
        raise HTTPException(status_code=404, detail=f"Todo {todo_id} not found")
    return Status(message=f"Deleted todo {todo_id}")