from tortoise.contrib.fastapi import register_tortoise

def tortoise_runner(app):
    return register_tortoise(
        app,
        db_url="postgres://postgres:1@localhost:5432/postgres",
        modules={"models": ["models.models"]},
        generate_schemas=True,
        add_exception_handlers=True,
    )
