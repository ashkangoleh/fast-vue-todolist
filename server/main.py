from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from views.views import app_router
from settings import db
import uvicorn



app = FastAPI(
    title="TODO"
)
db.tortoise_runner(app)

app.include_router(
    app_router,
    prefix="/api/v1",
    tags=['todo']
)

ORIGINS = [
    "http://localhost",
    "http://localhost:8080",
]
ALLOWED_HOSTS = [
    "*",
]
app.add_middleware(
    TrustedHostMiddleware, allowed_hosts=ALLOWED_HOSTS,
)
app.add_middleware(
    CORSMiddleware,
    allow_origins=ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)

