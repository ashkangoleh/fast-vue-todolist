from tortoise import fields, models
from tortoise.contrib.pydantic import pydantic_model_creator as pmg


class Todo(models.Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=255)
    completed = fields.BooleanField(default=False)


Todo_pydantic = pmg(Todo, name="Todo")
TodoIn_pydantic = pmg(Todo, name="TodoIn", exclude_readonly=True)
